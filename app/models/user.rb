class User < ApplicationRecord
  TEACHER = "teacher"
  STUDENT = "student"
  
  enum :kind, [:student, :teacher, :teacher_student]
  
  has_many :enrollments
  has_many :programs, through: :enrollments
  has_many :teacher_enrollments, class_name: 'Enrollment', foreign_key: 'teacher_id'

  before_update :validate_enrollment_present?

  def teachers
    User.joins(:teacher_enrollments).where(id: teacher_ids, teacher_enrollments: { user_id: id }).distinct
  end

  def teacher_ids
    teacher_ids = enrollments.pluck(:teacher_id)
  end

  class << self
    def favorites
      where(teacher_enrollments: { favorite: true })
    end

    def classmates(user)
      User.joins(:enrollments).where(
        enrollments: { program: user.programs }
      ).where.not(
        enrollments: { user_id: user.id }
      ).distinct
    end
  end

  private

  def validate_enrollment_present?
    return true unless kind_changed?

    if kind_change[0] == TEACHER
      teacher_enrollments.exists? ? log_error(kind: kind, action: "teaching") : true
    elsif kind_change[0] == STUDENT
      enrollments.exists? ? log_error(kind: kind, action: "studying") : true
    else
      true
    end
  end

  def log_error(kind:, action:)
    errors.add(:kind, I18n.t("user.error.kind.message", kind: kind, action: action))
    throw(:abort)
  end
end

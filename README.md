# Welcome to the assesment test
### DO NOT FORK THIS PROJECT

### We have 3 objects:
* A User that can either be a student (if it only learns courses), a teacher (if it only teaches)
or a student_teacher if it does both things

* A Program that represents the subject of a course (Eg: Calculus)

* An enrollment that represents the student and the teacher that are in a certain course 

### Before coming to the first tecnical meeting you:

* CAN NOT CHANGE THE TESTS or MIGRATIONS

* CAN NOT ADD NEW MODELS, CONTROLLERS or MIGRATIONS

* HAVE ALL RSPEC TESTS PASSING, without changing them (you can add more tests if you want)

* Have a basic idea of how the code works

* Understand how models should interact

### Changes Done:

* ALL RSPEC TESTS PASS

* Add changes to user model

* made enum with value student, teacher, teacher_student

* Made assocation for user with has_many Enrollments for user_id(enrollments), and teacher_id(teacher_enrollments)

* Made assocation for user for programs; Enrollments through programs to fetch program details for usrs

* Add class method to compute favorites and classmates of users

* add validation to validate_enrollment_present? in case of user kind update

* Add intance method to fetch user's teachers

